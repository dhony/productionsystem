﻿using ContosoUniversity.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ContosoUniversity.Data
{
    public class SchoolContext : DbContext
    {
        public SchoolContext(DbContextOptions<SchoolContext> options) : base(options)
        {
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<OfficeAssignment> OfficeAssignments { get; set; }
        public DbSet<CourseAssignment> CourseAssignments { get; set; }
        public DbSet<Person> People { get; set; }

        public DbSet<Truck> Truck { get; set; }
        public DbSet<LoadingCompany> LoadingCompany { get; set; }
        public DbSet<Stage> Stage { get; set; }
        public DbSet<Loader> Loader { get; set; }
        public DbSet<Crew> Crew { get; set; }
        public DbSet<Shift> Shift { get; set; }
        public DbSet<Level> Level { get; set; }
        public DbSet<Shot> Shot { get; set; }
        public DbSet<Pit> Pit { get; set; }
        public DbSet<MaterialOrigin> MaterialOrigin { get; set; }
        public DbSet<MaterialClass> MaterialClass { get; set; }
        public DbSet<MaterialClassClassification> MaterialClassClassification { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Instructor>().ToTable("Instructor");
            modelBuilder.Entity<OfficeAssignment>().ToTable("OfficeAssignment");
            modelBuilder.Entity<CourseAssignment>().ToTable("CourseAssignment");
            modelBuilder.Entity<Person>().ToTable("Person");

            modelBuilder.Entity<Truck>().ToTable("Truck");
            modelBuilder.Entity<LoadingCompany>().ToTable("LoadingCompany");
            modelBuilder.Entity<Stage>().ToTable("Stage");
            modelBuilder.Entity<Loader>().ToTable("Loader");
            modelBuilder.Entity<Crew>().ToTable("Crew");
            modelBuilder.Entity<Shift>().ToTable("Shift");
            modelBuilder.Entity<Level>().ToTable("Level");
            modelBuilder.Entity<Shot>().ToTable("Shot");
            modelBuilder.Entity<Pit>().ToTable("Pit");
            modelBuilder.Entity<MaterialOrigin>().ToTable("MaterialOrigin");
            modelBuilder.Entity<MaterialClass>().ToTable("MaterialClass");
            modelBuilder.Entity<MaterialClassClassification>().ToTable("MaterialClassClassification");


            modelBuilder.Entity<CourseAssignment>()
                .HasKey(c => new { c.CourseID, c.InstructorID });

        }
    }
}
