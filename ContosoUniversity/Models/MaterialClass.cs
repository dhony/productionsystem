﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class MaterialClass
    {
        public int MaterialClassID { get; set; }

        [Display(Name = "Material Class Classification")]
        public int MaterialClassClassificationID { get; set; }

        public MaterialClassClassification MaterialClassClassification { get; set; }

        public string Destination { get; set; }

        [Display(Name = "Is Pit Opened?")]
        public bool IsPitOpen { get; set; }
        [Display(Name = "Is Crush Opened?")]
        public bool IsCrushedOpen { get; set; }

        [Display(Name = "Opend Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime OpenedDate { get; set; }

        [Display(Name = "Closed Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ClosedDate { get; set; }
    }
}
