﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Shot
    {
        public int ShotID{ get; set; }

        [Display(Name = "Shot Sequency")]
        [DisplayFormat(DataFormatString = "{0:00}")] // to show the number with left 0
        public int ShotSequency { get; set; }

        [Display(Name = "Date Planned")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ShotDatePlanned { get; set; }

        [Display(Name = "Date Blasted")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? ShotDateBlasted { get; set; }

        public int LevelID { get; set; }
        public int StageID { get; set; }

        public Level Level { get; set; }
        public Stage Stage { get; set; }


        [Display(Name = "Cod Shot")]
        public string CodShot
        {
            get
            {
                return LevelID + "_" + StageID + (String.Format("{0:00}", ShotSequency)); // to show the ShotNumber with left 0
            }
        }
    }
}
