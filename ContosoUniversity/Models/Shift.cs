﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Shift
    {
        public int ShiftID { get; set; }

        [Display(Name = "Shift")]
        public string ShiftName { get; set; }

        [Display(Name = "Description")]
        public string ShiftDescription { get; set; }
    }
}
