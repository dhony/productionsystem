﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Crew
    {
        public int CrewID { get; set; }

        [Display(Name = "Crew")]
        public string CrewName { get; set; }

        [Display(Name = "Description")]
        public string CrewDescription { get; set; }

    }
}
