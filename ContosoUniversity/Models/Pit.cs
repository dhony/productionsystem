﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public enum Bench
    {
        A, B, C, D, F
    }

    public class Pit
    {
        public int PitID { get; set; }

        [Display(Name = "Date Created")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime PitDateCreated { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? PitDate { get; set; }

        [Display(Name = "Shift")]
        public int ShiftID { get; set; }
        [Display(Name = "Crew")]
        public int CrewID { get; set; }
        [Display(Name = "Truck")]
        public int TruckID { get; set; }
        [Display(Name = "Loader")]
        public int LoaderID { get; set; }
        [Display(Name = "Shot")]
        public int ShotID { get; set; }
        [Display(Name = "Material Origin")]
        public int MaterialOriginID { get; set; }

        [Display(Name = "Destination")]
        public int MaterialClassID { get; set; }

        public Shift Shift { get; set; }
        public Crew Crew { get; set; }
        public Truck Truck { get; set; }
        public Loader Loader { get; set; }
        public Shot Shot { get; set; }
        public MaterialOrigin MaterialOrigin { get; set; }
        public MaterialClass MaterialClass { get; set; }


        [Display(Name = "Quantity of Loads")]
        public int PitLoadCount { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
