﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class LoadingCompany
    {
        public int LoadingCompanyID { get; set; }

        [Display(Name = "Company")]
        public string LoadingCompanyName { get; set; }

        [Display(Name = "Description")]
        public string LoadCompanyDescription { get; set; }


        [Display(Name = "Start Operate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LoadingCompanyStartOperate { get; set; }


        public ICollection<Truck> Trucks { get; set; }
    }
}
