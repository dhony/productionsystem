﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Level
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Level")]
        public int LevelID { get; set; }

        [Display(Name = "Description")]
        public string LevelDescription { get; set; }
    }
}
