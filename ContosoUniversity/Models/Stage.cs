﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Stage
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Stage")]
        public int StageID { get; set; }

        [Display(Name = "Description")]
        public string StageDescription { get; set; }

    }
}
