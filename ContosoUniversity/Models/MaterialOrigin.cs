﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class MaterialOrigin
    {
        public int MaterialOriginID { get; set; }

        [Display(Name = "Date Created")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime MaterialOriginDateCreated { get; set; }

        [Display(Name = "Material Origin")]
        public string MaterialOriginName { get; set; }
    }
}
