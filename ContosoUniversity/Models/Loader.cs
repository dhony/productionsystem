﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Loader
    {
        public int LoaderID { get; set; }

        [Display(Name = "Loader")]
        public string LoaderName { get; set; }

        [Display(Name = "Description")]
        public string LoaderDescription { get; set; }

        [Display(Name = "Type")]
        public string LoaderType { get; set; }

        [Display(Name = "Start Operate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LoaderDateStartOperate { get; set; }
    }
}