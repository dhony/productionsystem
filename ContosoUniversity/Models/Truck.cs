﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class Truck
    {
        public int TruckID { get; set; }

        [Display(Name = "Truck")]
        public string TruckName { get; set; }

        [Display(Name = "Description")]
        public string TruckDescription { get; set; }

        [Display(Name = "Type")]
        public string TruckType { get; set; }

        [Display(Name = "Start Operate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime TruckDateStartOperate { get; set; }

        public int LoadingCompanyID { get; set; }

        public LoadingCompany LoadingCompany { get; set; }
    }
}
