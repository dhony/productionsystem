﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{
    public class MaterialClassClassification
    {
        public int MaterialClassClassificationID { get; set; }

        [Display(Name = "Material Class Classification")]
        public string MaterialClassClassificationName { get; set; }
    }
}
