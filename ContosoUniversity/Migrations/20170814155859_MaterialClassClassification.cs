﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class MaterialClassClassification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialOriginID",
                table: "MaterialClass");

            migrationBuilder.RenameColumn(
                name: "MaterialOriginID",
                table: "MaterialClass",
                newName: "MaterialClassClassificationID");

            migrationBuilder.RenameIndex(
                name: "IX_MaterialClass_MaterialOriginID",
                table: "MaterialClass",
                newName: "IX_MaterialClass_MaterialClassClassificationID");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialClassClassificationID",
                table: "MaterialClass",
                column: "MaterialClassClassificationID",
                principalTable: "MaterialOrigin",
                principalColumn: "MaterialOriginID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialClassClassificationID",
                table: "MaterialClass");

            migrationBuilder.RenameColumn(
                name: "MaterialClassClassificationID",
                table: "MaterialClass",
                newName: "MaterialOriginID");

            migrationBuilder.RenameIndex(
                name: "IX_MaterialClass_MaterialClassClassificationID",
                table: "MaterialClass",
                newName: "IX_MaterialClass_MaterialOriginID");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialOriginID",
                table: "MaterialClass",
                column: "MaterialOriginID",
                principalTable: "MaterialOrigin",
                principalColumn: "MaterialOriginID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
