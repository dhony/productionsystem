﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContosoUniversity.Migrations
{
    public partial class LoadingCompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "LoadingCompanyID",
            //    table: "Truck",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "LoadingCompany",
                columns: table => new
                {
                    LoadingCompanyID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LoadCompanyDescription = table.Column<string>(nullable: true),
                    LoadingCompanyName = table.Column<string>(nullable: true),
                    LoadingCompanyStartOperate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoadingCompany", x => x.LoadingCompanyID);
                });


            migrationBuilder.Sql("INSERT INTO dbo.LoadingCompany (LoadingCompanyName, LoadCompanyDescription, LoadingCompanyStartOperate) VALUES ('Temp', 'Description', GETDATE())");
            // Default value for FK points to department created above, with
            // defaultValue changed to 1 in following AddColumn statement.

            migrationBuilder.AddColumn<int>(
                name: "LoadingCompanyID",
                table: "Truck",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateIndex(
                name: "IX_Truck_LoadingCompanyID",
                table: "Truck",
                column: "LoadingCompanyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Truck_LoadingCompany_LoadingCompanyID",
                table: "Truck",
                column: "LoadingCompanyID",
                principalTable: "LoadingCompany",
                principalColumn: "LoadingCompanyID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Truck_LoadingCompany_LoadingCompanyID",
                table: "Truck");

            migrationBuilder.DropTable(
                name: "LoadingCompany");

            migrationBuilder.DropIndex(
                name: "IX_Truck_LoadingCompanyID",
                table: "Truck");

            migrationBuilder.DropColumn(
                name: "LoadingCompanyID",
                table: "Truck");
        }
    }
}
