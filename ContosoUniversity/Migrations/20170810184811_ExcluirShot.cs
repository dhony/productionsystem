﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class ExcluirShot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Shot");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Shot",
                columns: table => new
                {
                    LevelID = table.Column<int>(nullable: false),
                    StageID = table.Column<int>(nullable: false),
                    ShotNumber = table.Column<int>(nullable: false),
                    ShotDateBlasted = table.Column<DateTime>(nullable: false),
                    ShotDatePlanned = table.Column<DateTime>(nullable: false),
                    ShotID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shot", x => new { x.LevelID, x.StageID, x.ShotNumber });
                    table.UniqueConstraint("AK_Shot_ShotID", x => x.ShotID);
                    table.ForeignKey(
                        name: "FK_Shot_Level_LevelID",
                        column: x => x.LevelID,
                        principalTable: "Level",
                        principalColumn: "LevelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Shot_Stage_StageID",
                        column: x => x.StageID,
                        principalTable: "Stage",
                        principalColumn: "StageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shot_StageID",
                table: "Shot",
                column: "StageID");
        }
    }
}
