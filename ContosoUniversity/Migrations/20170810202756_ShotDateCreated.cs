﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContosoUniversity.Migrations
{
    public partial class ShotDateCreated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Shot",
                columns: table => new
                {
                    ShotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelID = table.Column<int>(nullable: false),
                    ShotDateBlasted = table.Column<DateTime>(nullable: true),
                    ShotDatePlanned = table.Column<DateTime>(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    ShotSequency = table.Column<int>(nullable: false),
                    StageID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shot", x => x.ShotID);
                    table.ForeignKey(
                        name: "FK_Shot_Level_LevelID",
                        column: x => x.LevelID,
                        principalTable: "Level",
                        principalColumn: "LevelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Shot_Stage_StageID",
                        column: x => x.StageID,
                        principalTable: "Stage",
                        principalColumn: "StageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shot_LevelID",
                table: "Shot",
                column: "LevelID");

            migrationBuilder.CreateIndex(
                name: "IX_Shot_StageID",
                table: "Shot",
                column: "StageID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Shot");
        }
    }
}
