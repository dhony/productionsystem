﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContosoUniversity.Migrations
{
    public partial class MaterialClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MaterialClass",
                columns: table => new
                {
                    MaterialClassID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClosedDate = table.Column<DateTime>(nullable: false),
                    Destination = table.Column<string>(nullable: true),
                    IsCrushedOpen = table.Column<bool>(nullable: false),
                    IsPitOpen = table.Column<bool>(nullable: false),
                    MaterialOriginID = table.Column<int>(nullable: false),
                    OpenedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialClass", x => x.MaterialClassID);
                    table.ForeignKey(
                        name: "FK_MaterialClass_MaterialOrigin_MaterialOriginID",
                        column: x => x.MaterialOriginID,
                        principalTable: "MaterialOrigin",
                        principalColumn: "MaterialOriginID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MaterialClassClassification",
                columns: table => new
                {
                    MaterialClassClassificationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MaterialClassClassificationName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialClassClassification", x => x.MaterialClassClassificationID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaterialClass_MaterialOriginID",
                table: "MaterialClass",
                column: "MaterialOriginID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterialClass");

            migrationBuilder.DropTable(
                name: "MaterialClassClassification");
        }
    }
}
