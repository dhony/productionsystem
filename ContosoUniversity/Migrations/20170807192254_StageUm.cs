﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class StageUm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "Stage",
               columns: table => new
               {
                   StageID = table.Column<int>(nullable: false),
                   StageDescription = table.Column<string>(nullable: true)

               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Stage", x => x.StageID);
               });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
