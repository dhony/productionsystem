﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContosoUniversity.Migrations
{
    public partial class PitString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pit",
                columns: table => new
                {
                    PitID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CrewID = table.Column<int>(nullable: false),
                    LoaderID = table.Column<int>(nullable: false),
                    PitDate = table.Column<DateTime>(nullable: true),
                    PitDateCreated = table.Column<DateTime>(nullable: false, defaultValueSql: "GETUTCDATE()"),
                    PitLoadCount = table.Column<int>(nullable: true),
                    ShiftID = table.Column<int>(nullable: false),
                    ShotID = table.Column<int>(nullable: false),
                    TruckID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pit", x => x.PitID);
                    table.ForeignKey(
                        name: "FK_Pit_Crew_CrewID",
                        column: x => x.CrewID,
                        principalTable: "Crew",
                        principalColumn: "CrewID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pit_Loader_LoaderID",
                        column: x => x.LoaderID,
                        principalTable: "Loader",
                        principalColumn: "LoaderID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pit_Shift_ShiftID",
                        column: x => x.ShiftID,
                        principalTable: "Shift",
                        principalColumn: "ShiftID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pit_Shot_ShotID",
                        column: x => x.ShotID,
                        principalTable: "Shot",
                        principalColumn: "ShotID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pit_Truck_TruckID",
                        column: x => x.TruckID,
                        principalTable: "Truck",
                        principalColumn: "TruckID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pit_CrewID",
                table: "Pit",
                column: "CrewID");

            migrationBuilder.CreateIndex(
                name: "IX_Pit_LoaderID",
                table: "Pit",
                column: "LoaderID");

            migrationBuilder.CreateIndex(
                name: "IX_Pit_ShiftID",
                table: "Pit",
                column: "ShiftID");

            migrationBuilder.CreateIndex(
                name: "IX_Pit_ShotID",
                table: "Pit",
                column: "ShotID");

            migrationBuilder.CreateIndex(
                name: "IX_Pit_TruckID",
                table: "Pit",
                column: "TruckID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
