﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class PitMaterialOrigin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            // Inserido manualmente de acordo com o tutorial

            migrationBuilder.Sql("INSERT INTO dbo.MaterialOrigin (MaterialOriginDateCreated, MaterialOriginName) VALUES (GETDATE(), 'O1')");
            // Default value for FK points to department created above, with
            // defaultValue changed to 1 in following AddColumn statement.


            migrationBuilder.AddColumn<int>(
                name: "MaterialOriginID",
                table: "Pit",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateIndex(
                name: "IX_Pit_MaterialOriginID",
                table: "Pit",
                column: "MaterialOriginID");

            migrationBuilder.AddForeignKey(
                name: "FK_Pit_MaterialOrigin_MaterialOriginID",
                table: "Pit",
                column: "MaterialOriginID",
                principalTable: "MaterialOrigin",
                principalColumn: "MaterialOriginID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pit_MaterialOrigin_MaterialOriginID",
                table: "Pit");

            migrationBuilder.DropIndex(
                name: "IX_Pit_MaterialOriginID",
                table: "Pit");

            migrationBuilder.DropColumn(
                name: "MaterialOriginID",
                table: "Pit");
        }
    }
}
