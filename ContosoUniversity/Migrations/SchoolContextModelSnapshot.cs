﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ContosoUniversity.Data;

namespace ContosoUniversity.Migrations
{
    [DbContext(typeof(SchoolContext))]
    partial class SchoolContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ContosoUniversity.Models.Course", b =>
                {
                    b.Property<int>("CourseID");

                    b.Property<int>("Credits");

                    b.Property<int>("DepartmentID");

                    b.Property<string>("Title")
                        .HasMaxLength(50);

                    b.HasKey("CourseID");

                    b.HasIndex("DepartmentID");

                    b.ToTable("Course");
                });

            modelBuilder.Entity("ContosoUniversity.Models.CourseAssignment", b =>
                {
                    b.Property<int>("CourseID");

                    b.Property<int>("InstructorID");

                    b.HasKey("CourseID", "InstructorID");

                    b.HasIndex("InstructorID");

                    b.ToTable("CourseAssignment");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Crew", b =>
                {
                    b.Property<int>("CrewID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CrewDescription");

                    b.Property<string>("CrewName");

                    b.HasKey("CrewID");

                    b.ToTable("Crew");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Department", b =>
                {
                    b.Property<int>("DepartmentID")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Budget")
                        .HasColumnType("money");

                    b.Property<int?>("InstructorID");

                    b.Property<string>("Name")
                        .HasMaxLength(50);

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("StartDate");

                    b.HasKey("DepartmentID");

                    b.HasIndex("InstructorID");

                    b.ToTable("Department");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Enrollment", b =>
                {
                    b.Property<int>("EnrollmentID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CourseID");

                    b.Property<int?>("Grade");

                    b.Property<int>("StudentID");

                    b.HasKey("EnrollmentID");

                    b.HasIndex("CourseID");

                    b.HasIndex("StudentID");

                    b.ToTable("Enrollment");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Level", b =>
                {
                    b.Property<int>("LevelID");

                    b.Property<string>("LevelDescription");

                    b.HasKey("LevelID");

                    b.ToTable("Level");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Loader", b =>
                {
                    b.Property<int>("LoaderID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("LoaderDateStartOperate");

                    b.Property<string>("LoaderDescription");

                    b.Property<string>("LoaderName");

                    b.Property<string>("LoaderType");

                    b.HasKey("LoaderID");

                    b.ToTable("Loader");
                });

            modelBuilder.Entity("ContosoUniversity.Models.LoadingCompany", b =>
                {
                    b.Property<int>("LoadingCompanyID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoadCompanyDescription");

                    b.Property<string>("LoadingCompanyName");

                    b.Property<DateTime>("LoadingCompanyStartOperate");

                    b.HasKey("LoadingCompanyID");

                    b.ToTable("LoadingCompany");
                });

            modelBuilder.Entity("ContosoUniversity.Models.MaterialClass", b =>
                {
                    b.Property<int>("MaterialClassID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("ClosedDate");

                    b.Property<string>("Destination");

                    b.Property<bool>("IsCrushedOpen");

                    b.Property<bool>("IsPitOpen");

                    b.Property<int>("MaterialClassClassificationID");

                    b.Property<DateTime>("OpenedDate");

                    b.HasKey("MaterialClassID");

                    b.HasIndex("MaterialClassClassificationID");

                    b.ToTable("MaterialClass");
                });

            modelBuilder.Entity("ContosoUniversity.Models.MaterialClassClassification", b =>
                {
                    b.Property<int>("MaterialClassClassificationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("MaterialClassClassificationName");

                    b.HasKey("MaterialClassClassificationID");

                    b.ToTable("MaterialClassClassification");
                });

            modelBuilder.Entity("ContosoUniversity.Models.MaterialOrigin", b =>
                {
                    b.Property<int>("MaterialOriginID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("MaterialOriginDateCreated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("MaterialOriginName");

                    b.HasKey("MaterialOriginID");

                    b.ToTable("MaterialOrigin");
                });

            modelBuilder.Entity("ContosoUniversity.Models.OfficeAssignment", b =>
                {
                    b.Property<int>("InstructorID");

                    b.Property<string>("Location")
                        .HasMaxLength(50);

                    b.HasKey("InstructorID");

                    b.ToTable("OfficeAssignment");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Person", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("FirstMidName")
                        .IsRequired()
                        .HasColumnName("FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("ID");

                    b.ToTable("Person");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Person");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Pit", b =>
                {
                    b.Property<int>("PitID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CrewID");

                    b.Property<int>("LoaderID");

                    b.Property<int>("MaterialClassID");

                    b.Property<int>("MaterialOriginID");

                    b.Property<DateTime?>("PitDate");

                    b.Property<DateTime>("PitDateCreated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<int>("PitLoadCount");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<int>("ShiftID");

                    b.Property<int>("ShotID");

                    b.Property<int>("TruckID");

                    b.HasKey("PitID");

                    b.HasIndex("CrewID");

                    b.HasIndex("LoaderID");

                    b.HasIndex("MaterialClassID");

                    b.HasIndex("MaterialOriginID");

                    b.HasIndex("ShiftID");

                    b.HasIndex("ShotID");

                    b.HasIndex("TruckID");

                    b.ToTable("Pit");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Shift", b =>
                {
                    b.Property<int>("ShiftID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ShiftDescription");

                    b.Property<string>("ShiftName");

                    b.HasKey("ShiftID");

                    b.ToTable("Shift");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Shot", b =>
                {
                    b.Property<int>("ShotID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LevelID");

                    b.Property<DateTime?>("ShotDateBlasted");

                    b.Property<DateTime>("ShotDatePlanned")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<int>("ShotSequency");

                    b.Property<int>("StageID");

                    b.HasKey("ShotID");

                    b.HasIndex("LevelID");

                    b.HasIndex("StageID");

                    b.ToTable("Shot");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Stage", b =>
                {
                    b.Property<int>("StageID");

                    b.Property<string>("StageDescription");

                    b.HasKey("StageID");

                    b.ToTable("Stage");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Truck", b =>
                {
                    b.Property<int>("TruckID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LoadingCompanyID");

                    b.Property<DateTime>("TruckDateStartOperate");

                    b.Property<string>("TruckDescription");

                    b.Property<string>("TruckName");

                    b.Property<string>("TruckType");

                    b.HasKey("TruckID");

                    b.HasIndex("LoadingCompanyID");

                    b.ToTable("Truck");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Instructor", b =>
                {
                    b.HasBaseType("ContosoUniversity.Models.Person");

                    b.Property<DateTime>("HireDate");

                    b.ToTable("Instructor");

                    b.HasDiscriminator().HasValue("Instructor");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Student", b =>
                {
                    b.HasBaseType("ContosoUniversity.Models.Person");

                    b.Property<DateTime>("EnrollmentDate");

                    b.ToTable("Student");

                    b.HasDiscriminator().HasValue("Student");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Course", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Department", "Department")
                        .WithMany("Courses")
                        .HasForeignKey("DepartmentID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.CourseAssignment", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Course", "Course")
                        .WithMany("CourseAssignments")
                        .HasForeignKey("CourseID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Instructor", "Instructor")
                        .WithMany("CourseAssignments")
                        .HasForeignKey("InstructorID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.Department", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Instructor", "Administrator")
                        .WithMany()
                        .HasForeignKey("InstructorID");
                });

            modelBuilder.Entity("ContosoUniversity.Models.Enrollment", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Course", "Course")
                        .WithMany("Enrollments")
                        .HasForeignKey("CourseID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Student", "Student")
                        .WithMany("Enrollments")
                        .HasForeignKey("StudentID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.MaterialClass", b =>
                {
                    b.HasOne("ContosoUniversity.Models.MaterialClassClassification", "MaterialClassClassification")
                        .WithMany()
                        .HasForeignKey("MaterialClassClassificationID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.OfficeAssignment", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Instructor", "Instructor")
                        .WithOne("OfficeAssignment")
                        .HasForeignKey("ContosoUniversity.Models.OfficeAssignment", "InstructorID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.Pit", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Crew", "Crew")
                        .WithMany()
                        .HasForeignKey("CrewID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Loader", "Loader")
                        .WithMany()
                        .HasForeignKey("LoaderID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.MaterialClass", "MaterialClass")
                        .WithMany()
                        .HasForeignKey("MaterialClassID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.MaterialOrigin", "MaterialOrigin")
                        .WithMany()
                        .HasForeignKey("MaterialOriginID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Shift", "Shift")
                        .WithMany()
                        .HasForeignKey("ShiftID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Shot", "Shot")
                        .WithMany()
                        .HasForeignKey("ShotID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Truck", "Truck")
                        .WithMany()
                        .HasForeignKey("TruckID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.Shot", b =>
                {
                    b.HasOne("ContosoUniversity.Models.Level", "Level")
                        .WithMany()
                        .HasForeignKey("LevelID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ContosoUniversity.Models.Stage", "Stage")
                        .WithMany()
                        .HasForeignKey("StageID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ContosoUniversity.Models.Truck", b =>
                {
                    b.HasOne("ContosoUniversity.Models.LoadingCompany", "LoadingCompany")
                        .WithMany("Trucks")
                        .HasForeignKey("LoadingCompanyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
