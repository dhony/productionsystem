﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class MaterialClassClassification_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialClassClassificationID",
                table: "MaterialClass");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialClass_MaterialClassClassification_MaterialClassClassificationID",
                table: "MaterialClass",
                column: "MaterialClassClassificationID",
                principalTable: "MaterialClassClassification",
                principalColumn: "MaterialClassClassificationID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaterialClass_MaterialClassClassification_MaterialClassClassificationID",
                table: "MaterialClass");

            migrationBuilder.AddForeignKey(
                name: "FK_MaterialClass_MaterialOrigin_MaterialClassClassificationID",
                table: "MaterialClass",
                column: "MaterialClassClassificationID",
                principalTable: "MaterialOrigin",
                principalColumn: "MaterialOriginID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
