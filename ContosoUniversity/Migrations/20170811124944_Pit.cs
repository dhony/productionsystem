﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ContosoUniversity.Migrations
{
    public partial class Pit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "PitDateCreated",
                table: "Pit",
                defaultValueSql: "GETUTCDATE()");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
