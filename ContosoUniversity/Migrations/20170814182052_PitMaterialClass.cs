﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContosoUniversity.Migrations
{
    public partial class PitMaterialClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Inserido manualmente de acordo com o tutorial

            migrationBuilder.Sql("INSERT INTO dbo.MaterialOrigin (MaterialOriginDateCreated, MaterialOriginName) VALUES (GETDATE(), 'O1')");
            // Default value for FK points to department created above, with
            // defaultValue changed to 1 in following AddColumn statement.

            migrationBuilder.AddColumn<int>(
                name: "MaterialClassID",
                table: "Pit",
                nullable: false,
                defaultValue: 2);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ClosedDate",
                table: "MaterialClass",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.CreateIndex(
                name: "IX_Pit_MaterialClassID",
                table: "Pit",
                column: "MaterialClassID");

            migrationBuilder.AddForeignKey(
                name: "FK_Pit_MaterialClass_MaterialClassID",
                table: "Pit",
                column: "MaterialClassID",
                principalTable: "MaterialClass",
                principalColumn: "MaterialClassID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pit_MaterialClass_MaterialClassID",
                table: "Pit");

            migrationBuilder.DropIndex(
                name: "IX_Pit_MaterialClassID",
                table: "Pit");

            migrationBuilder.DropColumn(
                name: "MaterialClassID",
                table: "Pit");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ClosedDate",
                table: "MaterialClass",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
