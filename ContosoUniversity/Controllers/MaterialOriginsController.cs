using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class MaterialOriginsController : Controller
    {
        private readonly SchoolContext _context;

        public MaterialOriginsController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: MaterialOrigins
        public async Task<IActionResult> Index()
        {
            return View(await _context.MaterialOrigin.ToListAsync());
        }

        // GET: MaterialOrigins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialOrigin = await _context.MaterialOrigin
                .SingleOrDefaultAsync(m => m.MaterialOriginID == id);
            if (materialOrigin == null)
            {
                return NotFound();
            }

            return View(materialOrigin);
        }

        // GET: MaterialOrigins/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MaterialOrigins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaterialOriginID,MaterialOriginDateCreated,MaterialOriginName")] MaterialOrigin materialOrigin)
        {
            if (ModelState.IsValid)
            {
                _context.Add(materialOrigin);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(materialOrigin);
        }

        // GET: MaterialOrigins/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialOrigin = await _context.MaterialOrigin.SingleOrDefaultAsync(m => m.MaterialOriginID == id);
            if (materialOrigin == null)
            {
                return NotFound();
            }
            return View(materialOrigin);
        }

        // POST: MaterialOrigins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaterialOriginID,MaterialOriginDateCreated,MaterialOriginName")] MaterialOrigin materialOrigin)
        {
            if (id != materialOrigin.MaterialOriginID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(materialOrigin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MaterialOriginExists(materialOrigin.MaterialOriginID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(materialOrigin);
        }

        // GET: MaterialOrigins/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialOrigin = await _context.MaterialOrigin
                .SingleOrDefaultAsync(m => m.MaterialOriginID == id);
            if (materialOrigin == null)
            {
                return NotFound();
            }

            return View(materialOrigin);
        }

        // POST: MaterialOrigins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var materialOrigin = await _context.MaterialOrigin.SingleOrDefaultAsync(m => m.MaterialOriginID == id);
            _context.MaterialOrigin.Remove(materialOrigin);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool MaterialOriginExists(int id)
        {
            return _context.MaterialOrigin.Any(e => e.MaterialOriginID == id);
        }
    }
}
