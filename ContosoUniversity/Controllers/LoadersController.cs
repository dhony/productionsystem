using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class LoadersController : Controller
    {
        private readonly SchoolContext _context;

        public LoadersController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: Loaders
        public async Task<IActionResult> Index()
        {
            return View(await _context.Loader.ToListAsync());
        }

        // GET: Loaders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loader = await _context.Loader
                .SingleOrDefaultAsync(m => m.LoaderID == id);
            if (loader == null)
            {
                return NotFound();
            }

            return View(loader);
        }

        // GET: Loaders/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Loaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LoaderID,LoaderName,LoaderDescription,LoaderType,LoaderDateStartOperate")] Loader loader)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loader);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(loader);
        }

        // GET: Loaders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loader = await _context.Loader.SingleOrDefaultAsync(m => m.LoaderID == id);
            if (loader == null)
            {
                return NotFound();
            }
            return View(loader);
        }

        // POST: Loaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LoaderID,LoaderName,LoaderDescription,LoaderType,LoaderDateStartOperate")] Loader loader)
        {
            if (id != loader.LoaderID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loader);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoaderExists(loader.LoaderID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(loader);
        }

        // GET: Loaders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loader = await _context.Loader
                .SingleOrDefaultAsync(m => m.LoaderID == id);
            if (loader == null)
            {
                return NotFound();
            }

            return View(loader);
        }

        // POST: Loaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loader = await _context.Loader.SingleOrDefaultAsync(m => m.LoaderID == id);
            _context.Loader.Remove(loader);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoaderExists(int id)
        {
            return _context.Loader.Any(e => e.LoaderID == id);
        }
    }
}
