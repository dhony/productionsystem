using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class PitsController : Controller
    {
        private readonly SchoolContext _context;

        public PitsController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: Pits
        public async Task<IActionResult> Index()
        {
            var schoolContext = _context.Pit.Include(p => p.Crew).Include(p => p.Loader).Include(p => p.Shift).Include(p => p.Shot).Include(p => p.Truck).Include(p => p.MaterialOrigin).Include(p => p.MaterialClass);
            return View(await schoolContext.ToListAsync());
        }

        // GET: Pits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pit = await _context.Pit
                .Include(p => p.Crew)
                .Include(p => p.Loader)
                .Include(p => p.Shift)
                .Include(p => p.Shot)
                .Include(p => p.Truck)
                .SingleOrDefaultAsync(m => m.PitID == id);
            if (pit == null)
            {
                return NotFound();
            }

            return View(pit);
        }

        // GET: Pits/Create
        public IActionResult Create()
        {
            //ViewData["CrewID"] = new SelectList(_context.Crew, "CrewID", "CrewID");
            PopulateCrewsDropDownList();
            //ViewData["LoaderID"] = new SelectList(_context.Loader, "LoaderID", "LoaderID");
            PopulateLoadersDropDownList();
            //ViewData["ShiftID"] = new SelectList(_context.Shift, "ShiftID", "ShiftID");
            PopulateShiftsDropDownList();
            //ViewData["ShotID"] = new SelectList(_context.Shot, "ShotID", "ShotID");
            PopulateShotsDropDownList();
            //ViewData["TruckID"] = new SelectList(_context.Truck, "TruckID", "TruckID");
            PopulateTrucksDropDownList();
            PopulateMaterialOriginsDropDownList();
            PopulateMaterialClassDropDownList();
            return View();
        }

        // POST: Pits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PitID,PitDateCreated,PitDate,ShiftID,CrewID,TruckID,LoaderID,ShotID,MaterialOriginID,PitLoadCount")] Pit pit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pit);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewData["CrewID"] = new SelectList(_context.Crew, "CrewID", "CrewID", pit.CrewID);
            PopulateCrewsDropDownList(pit.CrewID);
            //ViewData["LoaderID"] = new SelectList(_context.Loader, "LoaderID", "LoaderID", pit.LoaderID);
            PopulateLoadersDropDownList(pit.LoaderID);
            //ViewData["ShiftID"] = new SelectList(_context.Shift, "ShiftID", "ShiftID", pit.ShiftID);
            PopulateShiftsDropDownList(pit.PitID);
            //ViewData["ShotID"] = new SelectList(_context.Shot, "ShotID", "ShotID", pit.ShotID);
            PopulateShotsDropDownList(pit.ShotID);
            //ViewData["TruckID"] = new SelectList(_context.Truck, "TruckID", "TruckID", pit.TruckID);
            PopulateTrucksDropDownList(pit.TruckID);
            PopulateMaterialOriginsDropDownList(pit.MaterialOriginID);
            PopulateMaterialClassDropDownList(pit.MaterialClassID);
            return View(pit);
        }

        // GET: Pits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pit = await _context.Pit
                .Include(i => i.Crew)
                .Include(i => i.Loader)
                .Include(i => i.Shift)
                .Include(i => i.Shot)
                .Include(i => i.Truck)
                .Include(i => i.MaterialOrigin)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.PitID == id);
            if (pit == null)
            {
                return NotFound();
            }
            ViewData["CrewID"] = new SelectList(_context.Crew, "CrewID", "CrewName", pit.CrewID);
            ViewData["LoaderID"] = new SelectList(_context.Loader, "LoaderID", "LoaderName", pit.LoaderID);
            ViewData["ShiftID"] = new SelectList(_context.Shift, "ShiftID", "ShiftName", pit.ShiftID);
            ViewData["ShotID"] = new SelectList(_context.Shot, "ShotID", "CodShot", pit.ShotID);
            ViewData["TruckID"] = new SelectList(_context.Truck, "TruckID", "TruckName", pit.TruckID);
            ViewData["MaterialOriginID"] = new SelectList(_context.MaterialOrigin, "MaterialOriginID", "MaterialOriginName", pit.MaterialOriginID);
            ViewData["MaterialClassID"] = new SelectList(_context.MaterialClass, "MaterialClassID", "Destination", pit.MaterialClassID);
            return View(pit);
        }

        // POST: Pits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, byte[] rowVersion)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pitToUpdate = await _context.Pit
                .Include(i => i.Crew)
                .Include(i => i.Loader)
                .Include(i => i.Shift)
                .Include(i => i.Shot)
                .Include(i => i.Truck)
                .Include(i => i.MaterialOrigin)
                .Include(i => i.MaterialClass)
                .SingleOrDefaultAsync(c => c.PitID == id);

            if (pitToUpdate == null)
            {
                Pit deletedPit = new Pit();
                await TryUpdateModelAsync(deletedPit);
                ModelState.AddModelError(string.Empty,
                    "Unable to save changes. The department was deleted by another user.");
                ViewData["CrewID"] = new SelectList(_context.Crew, "CrewID", "CrewName", deletedPit.CrewID);
                ViewData["LoaderID"] = new SelectList(_context.Loader, "LoaderID", "LoaderName", deletedPit.LoaderID);
                ViewData["ShiftID"] = new SelectList(_context.Shift, "ShiftID", "ShiftName", deletedPit.ShiftID);
                ViewData["ShotID"] = new SelectList(_context.Shot, "ShotID", "CodShot", deletedPit.ShotID);
                ViewData["TruckID"] = new SelectList(_context.Truck, "TruckID", "TruckName", deletedPit.TruckID);
                ViewData["MaterialOriginID"] = new SelectList(_context.Truck, "MaterialOriginID", "MaterialOriginName", deletedPit.MaterialOriginID);
                return View(deletedPit);
            }

            _context.Entry(pitToUpdate).Property("RowVersion").OriginalValue = rowVersion;

            if (await TryUpdateModelAsync<Pit>(
                pitToUpdate,
                "",
                c => c.PitDate, c => c.ShiftID, c => c.CrewID, c => c.TruckID, c => c.LoaderID, c => c.MaterialOriginID, c => c.MaterialOriginID, c => c.PitLoadCount))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var exceptionEntry = ex.Entries.Single();
                    var clientValues = (Pit)exceptionEntry.Entity;
                    var databaseEntry = exceptionEntry.GetDatabaseValues();
                    if (databaseEntry == null)
                    {
                        ModelState.AddModelError(string.Empty,
                            "Unable to save changes. The pit was deleted by another user.");
                    }
                    else
                    {
                        var databaseValues = (Pit)databaseEntry.ToObject();

                        if (databaseValues.PitDateCreated != clientValues.PitDateCreated)
                        {
                            ModelState.AddModelError("PitDateCreated", $"Current value: {databaseValues.PitDateCreated}");
                        }
                        if (databaseValues.PitDate != clientValues.PitDate)
                        {
                            ModelState.AddModelError("PitDate", $"Current value: {databaseValues.PitDate}");
                        }
                        if (databaseValues.ShiftID != clientValues.ShiftID)
                        {
                            ModelState.AddModelError("ShiftID", $"Current value: {databaseValues.ShiftID}");
                        }
                        if (databaseValues.CrewID != clientValues.CrewID)
                        {
                            ModelState.AddModelError("CrewID", $"Current value: {databaseValues.CrewID}");
                        }
                        if (databaseValues.TruckID != clientValues.TruckID)
                        {
                            ModelState.AddModelError("TruckID", $"Current value: {databaseValues.TruckID}");
                        }
                        if (databaseValues.LoaderID != clientValues.LoaderID)
                        {
                            ModelState.AddModelError("LoaderID", $"Current value: {databaseValues.LoaderID}");
                        }
                        if (databaseValues.ShotID != clientValues.ShotID)
                        {
                            ModelState.AddModelError("ShotID", $"Current value: {databaseValues.ShotID}");
                        }
                        if (databaseValues.ShotID != clientValues.ShotID)
                        {
                            ModelState.AddModelError("MaterialOriginID", $"Current value: {databaseValues.MaterialOriginID}");
                        }
                        if (databaseValues.PitLoadCount != clientValues.PitLoadCount)
                        {
                            ModelState.AddModelError("PitLoadCount", $"Current value: {databaseValues.PitLoadCount}");
                        }

                        ModelState.AddModelError(string.Empty, "The record you attempted to edit "
                                + "was modified by another user after you got the original value. The "
                                + "edit operation was canceled and the current values in the database "
                                + "have been displayed. If you still want to edit this record, click "
                                + "the Save button again. Otherwise click the Back to List hyperlink.");
                        pitToUpdate.RowVersion = (byte[])databaseValues.RowVersion;
                        ModelState.Remove("RowVersion");
                    }
                }
            }
            ViewData["CrewID"] = new SelectList(_context.Crew, "CrewID", "CrewName", pitToUpdate.CrewID);
            ViewData["LoaderID"] = new SelectList(_context.Loader, "LoaderID", "LoaderName", pitToUpdate.LoaderID);
            ViewData["ShiftID"] = new SelectList(_context.Shift, "ShiftID", "ShiftName", pitToUpdate.ShiftID);
            ViewData["ShotID"] = new SelectList(_context.Shot, "ShotID", "CodShot", pitToUpdate.ShotID);
            ViewData["TruckID"] = new SelectList(_context.Truck, "TruckID", "TruckName", pitToUpdate.TruckID);
            ViewData["MaterialOriginID"] = new SelectList(_context.MaterialOrigin, "MaterialOriginID", "MaterialOriginName", pitToUpdate.MaterialOriginID);
            return View(pitToUpdate);
        }



        // Shifts Drop Down List
        private void PopulateShiftsDropDownList(object selectedShift = null)
        {
            var shiftsQuery = from d in _context.Shift
                              orderby d.ShiftName
                              select d;
            ViewBag.ShiftID = new SelectList(shiftsQuery.AsNoTracking(), "ShiftID", "ShiftName", selectedShift);
        }

        // Crews Drop Down List
        private void PopulateCrewsDropDownList(object selectedCrew = null)
        {
            var crewsQuery = from d in _context.Crew
                              orderby d.CrewName
                              select d;
            ViewBag.CrewID = new SelectList(crewsQuery.AsNoTracking(), "CrewID", "CrewName", selectedCrew);
        }

        // Trucks Drop Down List
        private void PopulateTrucksDropDownList(object selectedTruck = null)
        {
            var trucksQuery = from d in _context.Truck
                             orderby d.TruckName
                             select d;
            ViewBag.TruckID = new SelectList(trucksQuery.AsNoTracking(), "TruckID", "TruckName", selectedTruck);
        }

        // Loader Drop Down List
        private void PopulateLoadersDropDownList(object selectedLoader = null)
        {
            var loadersQuery = from d in _context.Loader
                              orderby d.LoaderName
                              select d;
            ViewBag.LoaderID = new SelectList(loadersQuery.AsNoTracking(), "LoaderID", "LoaderName", selectedLoader);
        }

        // Shot Drop Down List
        private void PopulateShotsDropDownList(object selectedShot = null)
        {
            var shotsQuery = from d in _context.Shot
                               orderby d.CodShot
                               select d;
            ViewBag.ShotID = new SelectList(shotsQuery.AsNoTracking(), "ShotID", "CodShot", selectedShot);
        }

        // Material Origin Drop Down List
        private void PopulateMaterialOriginsDropDownList(object selectedMaterialOrigin = null)
        {
            var materialOriginQuery = from d in _context.MaterialOrigin
                             orderby d.MaterialOriginName
                             select d;
            ViewBag.MaterialOriginID = new SelectList(materialOriginQuery.AsNoTracking(), "MaterialOriginID", "MaterialOriginName", selectedMaterialOrigin);
        }

        // Material Class (Destination) Drop Down List
        private void PopulateMaterialClassDropDownList(object selectedMaterialClass = null)
        {
            var materialClassQuery = from d in _context.MaterialClass
                                      orderby d.Destination
                                      select d;
            ViewBag.MaterialClassID = new SelectList(materialClassQuery.AsNoTracking(), "MaterialClassID", "Destination", selectedMaterialClass);
        }


        // GET: Pits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pit = await _context.Pit
                .Include(p => p.Crew)
                .Include(p => p.Loader)
                .Include(p => p.Shift)
                .Include(p => p.Shot)
                .Include(p => p.Truck)
                .SingleOrDefaultAsync(m => m.PitID == id);
            if (pit == null)
            {
                return NotFound();
            }

            return View(pit);
        }

        // POST: Pits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pit = await _context.Pit.SingleOrDefaultAsync(m => m.PitID == id);
            _context.Pit.Remove(pit);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PitExists(int id)
        {
            return _context.Pit.Any(e => e.PitID == id);
        }
    }
}
