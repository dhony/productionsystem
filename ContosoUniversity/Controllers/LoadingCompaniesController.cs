using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class LoadingCompaniesController : Controller
    {
        private readonly SchoolContext _context;

        public LoadingCompaniesController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: LoadingCompanies
        public async Task<IActionResult> Index()
        {
            return View(await _context.LoadingCompany.ToListAsync());
        }

        // GET: LoadingCompanies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loadingCompany = await _context.LoadingCompany
                .SingleOrDefaultAsync(m => m.LoadingCompanyID == id);
            if (loadingCompany == null)
            {
                return NotFound();
            }

            return View(loadingCompany);
        }

        // GET: LoadingCompanies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoadingCompanies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LoadingCompanyID,LoadingCompanyName,LoadCompanyDescription,LoadingCompanyStartOperate")] LoadingCompany loadingCompany)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loadingCompany);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(loadingCompany);
        }

        // GET: LoadingCompanies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loadingCompany = await _context.LoadingCompany.SingleOrDefaultAsync(m => m.LoadingCompanyID == id);
            if (loadingCompany == null)
            {
                return NotFound();
            }
            return View(loadingCompany);
        }

        // POST: LoadingCompanies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LoadingCompanyID,LoadingCompanyName,LoadCompanyDescription,LoadingCompanyStartOperate")] LoadingCompany loadingCompany)
        {
            if (id != loadingCompany.LoadingCompanyID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loadingCompany);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoadingCompanyExists(loadingCompany.LoadingCompanyID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(loadingCompany);
        }

        // GET: LoadingCompanies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loadingCompany = await _context.LoadingCompany
                .SingleOrDefaultAsync(m => m.LoadingCompanyID == id);
            if (loadingCompany == null)
            {
                return NotFound();
            }

            return View(loadingCompany);
        }

        // POST: LoadingCompanies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loadingCompany = await _context.LoadingCompany.SingleOrDefaultAsync(m => m.LoadingCompanyID == id);
            _context.LoadingCompany.Remove(loadingCompany);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LoadingCompanyExists(int id)
        {
            return _context.LoadingCompany.Any(e => e.LoadingCompanyID == id);
        }
    }
}
