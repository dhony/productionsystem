using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class MaterialClassClassificationsController : Controller
    {
        private readonly SchoolContext _context;

        public MaterialClassClassificationsController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: MaterialClassClassifications
        public async Task<IActionResult> Index()
        {
            return View(await _context.MaterialClassClassification.ToListAsync());
        }

        // GET: MaterialClassClassifications/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClassClassification = await _context.MaterialClassClassification
                .SingleOrDefaultAsync(m => m.MaterialClassClassificationID == id);
            if (materialClassClassification == null)
            {
                return NotFound();
            }

            return View(materialClassClassification);
        }

        // GET: MaterialClassClassifications/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MaterialClassClassifications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaterialClassClassificationID,MaterialClassClassificationName")] MaterialClassClassification materialClassClassification)
        {
            if (ModelState.IsValid)
            {
                _context.Add(materialClassClassification);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(materialClassClassification);
        }

        // GET: MaterialClassClassifications/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClassClassification = await _context.MaterialClassClassification.SingleOrDefaultAsync(m => m.MaterialClassClassificationID == id);
            if (materialClassClassification == null)
            {
                return NotFound();
            }
            return View(materialClassClassification);
        }

        // POST: MaterialClassClassifications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaterialClassClassificationID,MaterialClassClassificationName")] MaterialClassClassification materialClassClassification)
        {
            if (id != materialClassClassification.MaterialClassClassificationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(materialClassClassification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MaterialClassClassificationExists(materialClassClassification.MaterialClassClassificationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(materialClassClassification);
        }

        // GET: MaterialClassClassifications/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClassClassification = await _context.MaterialClassClassification
                .SingleOrDefaultAsync(m => m.MaterialClassClassificationID == id);
            if (materialClassClassification == null)
            {
                return NotFound();
            }

            return View(materialClassClassification);
        }

        // POST: MaterialClassClassifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var materialClassClassification = await _context.MaterialClassClassification.SingleOrDefaultAsync(m => m.MaterialClassClassificationID == id);
            _context.MaterialClassClassification.Remove(materialClassClassification);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool MaterialClassClassificationExists(int id)
        {
            return _context.MaterialClassClassification.Any(e => e.MaterialClassClassificationID == id);
        }
    }
}
