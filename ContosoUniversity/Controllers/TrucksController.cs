using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class TrucksController : Controller
    {
        private readonly SchoolContext _context;

        public TrucksController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: Trucks
        public async Task<IActionResult> Index()
        {
            return View(await _context.Truck.ToListAsync());
        }

        // GET: Trucks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck = await _context.Truck
                .Include(c => c.LoadingCompany)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.TruckID == id);
            if (truck == null)
            {
                return NotFound();
            }

            return View(truck);
        }

        // GET: Trucks/Create
        public IActionResult Create()
        {
            PopulateLoadingCompaniesDropDownList();
            return View();
        }

        // POST: Trucks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TruckID,TruckName,TruckDescription,TruckType,LoadingCompanyID,TruckDateStartOperate")] Truck truck)
        {
            if (ModelState.IsValid)
            {
                _context.Add(truck);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            PopulateLoadingCompaniesDropDownList(truck.LoadingCompanyID);
            return View(truck);
        }

        // GET: Trucks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck = await _context.Truck
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.TruckID == id);
            if (truck == null)
            {
                return NotFound();
            }
            PopulateLoadingCompaniesDropDownList(); // ainda � necess�rio criar esse m�todo
            return View(truck);
        }

        // POST: Trucks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truckToUpdate = await _context.Truck
                .SingleOrDefaultAsync(c => c.TruckID == id);

            if (await TryUpdateModelAsync<Truck>(truckToUpdate,
                "",
                c => c.TruckName, c => c.TruckDescription, c => c.TruckType, c => c.TruckDateStartOperate, c => c.LoadingCompanyID))
            {
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
                return RedirectToAction("Index");
            }
            PopulateLoadingCompaniesDropDownList(truckToUpdate.LoadingCompanyID);
            return View(truckToUpdate);
        }

        // M�todo PopulateLoadingCompaniesDropDownList
        private void PopulateLoadingCompaniesDropDownList(object selectedLoadingCompany = null)
        {
            var companiesQuery = from d in _context.LoadingCompany
                                 orderby d.LoadingCompanyName
                                 select d;
            ViewBag.LoadingCompanyID = new SelectList(companiesQuery.AsNoTracking(), "LoadingCompanyID", "LoadingCompanyName", selectedLoadingCompany);
        }


        // GET: Trucks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var truck = await _context.Truck
                .Include(c => c.LoadingCompany)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.TruckID == id);
            if (truck == null)
            {
                return NotFound();
            }

            return View(truck);
        }

        // POST: Trucks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var truck = await _context.Truck.SingleOrDefaultAsync(m => m.TruckID == id);
            _context.Truck.Remove(truck);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TruckExists(int id)
        {
            return _context.Truck.Any(e => e.TruckID == id);
        }
    }
}
