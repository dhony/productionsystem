using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class ShotsController : Controller
    {
        private readonly SchoolContext _context;

        public ShotsController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: Shots
        public async Task<IActionResult> Index()
        {
            var schoolContext = _context.Shot.Include(s => s.Level).Include(s => s.Stage);
            return View(await schoolContext.ToListAsync());
        }

        // GET: Shots/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shot = await _context.Shot
                .Include(s => s.Level)
                .Include(s => s.Stage)
                .SingleOrDefaultAsync(m => m.ShotID == id);
            if (shot == null)
            {
                return NotFound();
            }

            return View(shot);
        }

        // GET: Shots/Create
        public IActionResult Create()
        {
            ViewData["LevelID"] = new SelectList(_context.Level, "LevelID", "LevelID");
            ViewData["StageID"] = new SelectList(_context.Stage, "StageID", "StageID");
            return View();
        }

        // POST: Shots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ShotID,ShotSequency,ShotDatePlanned,ShotDateBlasted,LevelID,StageID")] Shot shot)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shot);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["LevelID"] = new SelectList(_context.Level, "LevelID", "LevelID", shot.LevelID);
            ViewData["StageID"] = new SelectList(_context.Stage, "StageID", "StageID", shot.StageID);
            return View(shot);
        }

        // GET: Shots/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shot = await _context.Shot.SingleOrDefaultAsync(m => m.ShotID == id);
            if (shot == null)
            {
                return NotFound();
            }
            ViewData["LevelID"] = new SelectList(_context.Level, "LevelID", "LevelID", shot.LevelID);
            ViewData["StageID"] = new SelectList(_context.Stage, "StageID", "StageID", shot.StageID);
            return View(shot);
        }

        // POST: Shots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ShotID,ShotSequency,ShotDatePlanned,ShotDateBlasted,LevelID,StageID")] Shot shot)
        {
            if (id != shot.ShotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(shot);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShotExists(shot.ShotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["LevelID"] = new SelectList(_context.Level, "LevelID", "LevelID", shot.LevelID);
            ViewData["StageID"] = new SelectList(_context.Stage, "StageID", "StageID", shot.StageID);
            return View(shot);
        }

        // GET: Shots/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var shot = await _context.Shot
                .Include(s => s.Level)
                .Include(s => s.Stage)
                .SingleOrDefaultAsync(m => m.ShotID == id);
            if (shot == null)
            {
                return NotFound();
            }

            return View(shot);
        }

        // POST: Shots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var shot = await _context.Shot.SingleOrDefaultAsync(m => m.ShotID == id);
            _context.Shot.Remove(shot);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ShotExists(int id)
        {
            return _context.Shot.Any(e => e.ShotID == id);
        }
    }
}
