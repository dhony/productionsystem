using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;

namespace ContosoUniversity.Controllers
{
    public class MaterialClassesController : Controller
    {
        private readonly SchoolContext _context;

        public MaterialClassesController(SchoolContext context)
        {
            _context = context;    
        }

        // GET: MaterialClasses
        public async Task<IActionResult> Index()
        {
            var schoolContext = _context.MaterialClass.Include(m => m.MaterialClassClassification);
            return View(await schoolContext.ToListAsync());
        }

        // GET: MaterialClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClass = await _context.MaterialClass
                .Include(m => m.MaterialClassClassification)
                .SingleOrDefaultAsync(m => m.MaterialClassID == id);
            if (materialClass == null)
            {
                return NotFound();
            }

            return View(materialClass);
        }

        // GET: MaterialClasses/Create
        public IActionResult Create()
        {
            PopulateMaterialClassClassificationDropDownList();
            //ViewData["MaterialClassClassificationID"] = new SelectList(_context.MaterialClassClassification, "MaterialClassClassificationID", "MaterialClassClassificationID");
            return View();
        }

        // POST: MaterialClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaterialClassID,MaterialClassClassificationID,Destination,IsPitOpen,IsCrushedOpen,OpenedDate,ClosedDate")] MaterialClass materialClass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(materialClass);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["MaterialClassClassificationID"] = new SelectList(_context.MaterialClassClassification, "MaterialClassClassificationID", "MaterialClassClassificationID", materialClass.MaterialClassClassificationID);
            return View(materialClass);
        }

        // GET: MaterialClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClass = await _context.MaterialClass.SingleOrDefaultAsync(m => m.MaterialClassID == id);
            if (materialClass == null)
            {
                return NotFound();
            }
            ViewData["MaterialClassClassificationID"] = new SelectList(_context.MaterialClassClassification, "MaterialClassClassificationID", "MaterialClassClassificationID", materialClass.MaterialClassClassificationID);
            return View(materialClass);
        }

        // POST: MaterialClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaterialClassID,MaterialClassClassificationID,Destination,IsPitOpen,IsCrushedOpen,OpenedDate,ClosedDate")] MaterialClass materialClass)
        {
            if (id != materialClass.MaterialClassID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(materialClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MaterialClassExists(materialClass.MaterialClassID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["MaterialClassClassificationID"] = new SelectList(_context.MaterialClassClassification, "MaterialClassClassificationID", "MaterialClassClassificationID", materialClass.MaterialClassClassificationID);
            return View(materialClass);
        }


        // MaterialClassClassification Drop Down List
        private void PopulateMaterialClassClassificationDropDownList(object selectedMaterialClassClassification = null)
        {
            var materialClassClassificationQuery = from d in _context.MaterialClassClassification
                              orderby d.MaterialClassClassificationName
                              select d;
            ViewBag.MaterialClassClassificationID = new SelectList(materialClassClassificationQuery.AsNoTracking(), "MaterialClassClassificationID", "MaterialClassClassificationName", selectedMaterialClassClassification);
        }

        // GET: MaterialClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var materialClass = await _context.MaterialClass
                .Include(m => m.MaterialClassClassification)
                .SingleOrDefaultAsync(m => m.MaterialClassID == id);
            if (materialClass == null)
            {
                return NotFound();
            }

            return View(materialClass);
        }

        // POST: MaterialClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var materialClass = await _context.MaterialClass.SingleOrDefaultAsync(m => m.MaterialClassID == id);
            _context.MaterialClass.Remove(materialClass);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool MaterialClassExists(int id)
        {
            return _context.MaterialClass.Any(e => e.MaterialClassID == id);
        }
    }
}
